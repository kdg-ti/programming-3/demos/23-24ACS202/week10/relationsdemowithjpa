DROP TABLE IF EXISTS STUDENTS_COURSES;
DROP TABLE IF EXISTS ADDRESS;
DROP TABLE IF EXISTS STUDENTS;
DROP TABLE IF EXISTS SCHOOLS;
create table SCHOOLS
(
    ID   SERIAL PRIMARY KEY,
    NAME CHARACTER VARYING(100) not null
);


create table STUDENTS
(
    ID        SERIAL PRIMARY KEY,
    NAME      CHARACTER VARYING(100),
    LENGTH    DOUBLE PRECISION,
    BIRTHDAY  DATE,
    SCHOOL_ID INTEGER not null,
    constraint FK_STUDENTS_SCHOOL_ID
        foreign key (SCHOOL_ID) references SCHOOLS
);


create table ADDRESS
(
    STREET      CHARACTER VARYING(100) not null,
    POSTAL_CODE INTEGER                not null,
    CITY        CHARACTER VARYING(100) not null,
    STUDENT_ID  INTEGER                not null,
    constraint FK_ADRESS_STUDENT_ID
        foreign key (STUDENT_ID) references STUDENTS
);

DROP TABLE IF EXISTS COURSES;
create table COURSES
(
    ID            SERIAL PRIMARY KEY,
    NAME          CHARACTER VARYING(100) not null,
    ACADEMIC_YEAR INTEGER not null
);

create table STUDENTS_COURSES
(
    STUDENT_ID INTEGER not null,
    COURSE_ID  INTEGER not null,
    constraint FK_STUDENTS_COURSES_COURSE_ID
        foreign key (COURSE_ID) references COURSES,
    constraint FK_STUDENTS_COURSES_STUDENT_ID
        foreign key (STUDENT_ID) references STUDENTS
);

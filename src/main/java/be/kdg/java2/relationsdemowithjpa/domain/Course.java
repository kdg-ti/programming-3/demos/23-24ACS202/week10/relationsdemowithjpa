package be.kdg.java2.relationsdemowithjpa.domain;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "COURSES")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int academicYear;

    //Many-to-many relation
    @ManyToMany(mappedBy = "courses")//We don't use EAGER in this direction: loading the course will not load the students
    private List<Student> students = new ArrayList<>();

    protected Course() {

    }

    public Course(String name, int academicYear) {
        this.name = name;
        this.academicYear = academicYear;
    }

    public Course(int id, String name, int academicYear) {
        this.id = id;
        this.name = name;
        this.academicYear = academicYear;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(int academicYear) {
        this.academicYear = academicYear;
    }

    public void addStudent(Student student){
        students.add(student);
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", academicYear=" + academicYear +
                '}';
    }
}

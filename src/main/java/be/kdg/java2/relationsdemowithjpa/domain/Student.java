package be.kdg.java2.relationsdemowithjpa.domain;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="STUDENTS")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private double length;
    private LocalDate birthday;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "school_id")
    private School school;

    @ManyToMany(fetch = FetchType.EAGER)//we use eager in this direction: loading a student will load his/her courses
    @JoinTable(name = "STUDENTS_COURSES")
    private List<Course> courses = new ArrayList<>();

    protected Student() {
        System.out.println("creating student proxy...");
    }

    public Student(int id, String name, double length, LocalDate birthday) {
        this.id = id;
        this.name = name;
        this.length = length;
        this.birthday = birthday;
    }

    public Student(String name, double length, LocalDate birthday) {
        this.name = name;
        this.length = length;
        this.birthday = birthday;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double lenght) {
        this.length = lenght;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
        address.setStudent(this);
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
        school.addStudent(this);
    }

    public void addCourse(Course course){
        this.courses.add(course);
        course.addStudent(this);
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
        //should we for each course set the student?
        //this list of students in the course object will depend on wether the student was loaded from db
        //maybe better not to load?
        //courses.forEach(c->c.addStudent(this));
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lenght=" + length +
                ", birthday=" + birthday +
                '}' + (address!=null?"Address: " + address.toString():"<no address>")+
               ", " + (school!=null?"School: " + school.toString():"<no school>");
    }
}

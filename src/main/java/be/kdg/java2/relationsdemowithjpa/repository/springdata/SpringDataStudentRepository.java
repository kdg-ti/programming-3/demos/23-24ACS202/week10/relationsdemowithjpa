package be.kdg.java2.relationsdemowithjpa.repository.springdata;

import be.kdg.java2.relationsdemowithjpa.domain.Student;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Profile("springdata")
public interface SpringDataStudentRepository extends JpaRepository<Student, Integer> {
    @Query("select student from Student student join student.courses course where course.id = :courseId")
    List<Student> findByCourse(int courseId);
    @Query("select student from Student student where student.school.id = :schoolId")
    List<Student> findBySchool(int schoolId);
}

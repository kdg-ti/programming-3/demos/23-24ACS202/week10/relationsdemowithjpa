package be.kdg.java2.relationsdemowithjpa.repository.springdata;

import be.kdg.java2.relationsdemowithjpa.domain.School;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("springdata")
public interface SpringDataSchoolRepository extends JpaRepository<School,Integer> {
}

package be.kdg.java2.relationsdemowithjpa.repository.jpa;

import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.repository.CourseRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceUnit;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Profile("jpa")
public class JPACourseRepository implements CourseRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Course> findAll() {
        List<Course> courses = em.createQuery("select course from Course course", Course.class)
                .getResultList();
        return courses;
    }

    @Override
    public Course findById(int id) {
        Course course = em.find(Course.class, id);
        return course;
    }

    @Override
    public Course createCourse(Course course) {
        em.persist(course);
        return course;
    }

    @Override
    public void updateCourse(Course course) {
        em.merge(course);
    }

    @Override
    public void deleteCourse(int id) {
        em.remove(em.find(Course.class, id));
    }
}

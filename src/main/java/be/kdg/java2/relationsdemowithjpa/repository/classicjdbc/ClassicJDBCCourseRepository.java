package be.kdg.java2.relationsdemowithjpa.repository.classicjdbc;

import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.repository.CourseRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Profile("classic")
public class ClassicJDBCCourseRepository implements CourseRepository {
    @Override
    public List<Course> findAll() {
        return null;
    }

    @Override
    public Course findById(int id) {
        return null;
    }

    @Override
    public Course createCourse(Course course) {
        return null;
    }

    @Override
    public void updateCourse(Course course) {

    }

    @Override
    public void deleteCourse(int id) {

    }
}

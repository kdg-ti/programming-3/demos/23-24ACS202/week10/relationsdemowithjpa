package be.kdg.java2.relationsdemowithjpa.repository.jdbc;


import be.kdg.java2.relationsdemowithjpa.domain.Address;
import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.domain.School;
import be.kdg.java2.relationsdemowithjpa.domain.Student;
import be.kdg.java2.relationsdemowithjpa.repository.StudentRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("jdbc")
public class JDBCStudentRepository implements StudentRepository {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert studentInserter;

    public JDBCStudentRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.studentInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("STUDENTS")
                .usingGeneratedKeyColumns("ID");
    }

    //Helper method: maps the columns of the DB to the attributes of the Student
    public static Student mapStudentRow(ResultSet rs, int rowid) throws SQLException {
        return new Student(rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getDouble("LENGTH"),
                rs.getDate("BIRTHDAY").toLocalDate());
    }

    //Helper method: maps the columns of the DB to the attributes of the Address
    public static Address mapAddressRow(ResultSet rs, int rowid) throws SQLException {
        return new Address(
                rs.getString("STREET"),
                rs.getInt("POSTAL_CODE"),
                rs.getString("CITY"));
    }

    @Override
    public List<Student> findAll() {
        List<Student> students = jdbcTemplate.query("SELECT * FROM STUDENTS", JDBCStudentRepository::mapStudentRow);
        //load the addresses:
        students.forEach(this::loadAddressAndSchool);
        students.forEach(this::loadCourses);
        return students;
    }

    @Override
    public List<Student> findBySchool(int schoolid) {
        List<Student> students = jdbcTemplate.query("SELECT * FROM STUDENTS WHERE SCHOOL_ID = ?", JDBCStudentRepository::mapStudentRow, schoolid);
        //load the addresses:
        students.forEach(this::loadAddressAndSchool);
        students.forEach(this::loadCourses);
        return students;
    }

    @Override
    public List<Student> findByCourse(int courseId) {
        List<Student> students = jdbcTemplate.query("SELECT * FROM STUDENTS WHERE ID IN " +
                "(SELECT STUDENT_ID FROM STUDENTS_COURSES WHERE COURSE_ID = ?)", JDBCStudentRepository::mapStudentRow, courseId);
        //load the addresses:
        students.forEach(this::loadAddressAndSchool);
        students.forEach(this::loadCourses);
        return students;
    }

    @Override
    public Student findById(int id) {
        Student student = jdbcTemplate.queryForObject("SELECT * FROM STUDENTS WHERE ID = ?", JDBCStudentRepository::mapStudentRow, id);
        //load the address and school:
        if (student != null) {
            loadAddressAndSchool(student);
            //load the courses... --> this is eager loading!
            loadCourses(student);
        }
        return student;
    }

    private void loadCourses(Student student) {
        List<Course> courses = jdbcTemplate.query("SELECT * FROM COURSES WHERE ID IN " +
                "(SELECT COURSE_ID FROM STUDENTS_COURSES WHERE STUDENT_ID = ?) ", JDBCCourseRepository::mapCourseRow, student.getId());
        student.setCourses(courses);
    }

    private void loadAddressAndSchool(Student student) {
        try {
            Address address = jdbcTemplate.queryForObject("SELECT * FROM ADDRESS WHERE STUDENT_ID = ?", JDBCStudentRepository::mapAddressRow, student.getId());
            student.setAddress(address);
        } catch (EmptyResultDataAccessException e) {
            //no address found, do nothing...
        }
        try {
            School school = jdbcTemplate.queryForObject("SELECT SCHOOLS.ID, SCHOOLS.NAME " +
                    "FROM SCHOOLS INNER JOIN STUDENTS on SCHOOLS.ID = STUDENTS.SCHOOL_ID AND STUDENTS.ID = ?", JDBCSchoolRepository::mapSchoolRow, student.getId());
            student.setSchool(school);
        } catch (EmptyResultDataAccessException e) {
            //no school found, do nothing...
        }
    }

    @Override
    public Student createStudent(Student student) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", student.getName());
        parameters.put("LENGTH", student.getLength());
        parameters.put("BIRTHDAY", Date.valueOf(student.getBirthday()));
        parameters.put("SCHOOL_ID", student.getSchool().getId());
        student.setId(studentInserter.executeAndReturnKey(parameters).intValue());
        if (student.getAddress() != null) {
            jdbcTemplate.update("INSERT INTO ADDRESS (STREET, POSTAL_CODE, CITY, STUDENT_ID) VALUES (?, ?, ?,?)",
                    student.getAddress().getStreet(), student.getAddress().getPostalCode(), student.getAddress().getCity(), student.getId());
        }
        //school should already be created--> is "independent" entity
        return student;
    }

    @Override
    public void updateStudent(Student student) {
        jdbcTemplate.update("UPDATE STUDENTS SET NAME=?, LENGTH=?,BIRTHDAY=?, SCHOOL_ID=? WHERE ID=?",
                student.getName(), student.getLength(), Date.valueOf(student.getBirthday()), student.getSchool().getId(), student.getId());
        if (student.getAddress() != null) {
            jdbcTemplate.update("MERGE INTO ADDRESS (STREET, POSTAL_CODE, CITY, STUDENT_ID) KEY (STUDENT_ID) VALUES (?, ?, ?, ?)",
                    student.getAddress().getStreet(), student.getAddress().getPostalCode(), student.getAddress().getCity(), student.getId());
        }
        //what about the students courses??
        //we check the list of courses and update the crosstable?
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE STUDENT_ID = ?",student.getId());
        for (Course course : student.getCourses()) {
            jdbcTemplate.update("INSERT INTO STUDENTS_COURSES VALUES ( ?,? )",student.getId(),course.getId());
        }
    }

    @Override
    @Transactional
    public void deleteStudent(int id) {
        jdbcTemplate.update("DELETE FROM ADDRESS WHERE STUDENT_ID=?", id);
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE STUDENT_ID=?", id);
        jdbcTemplate.update("DELETE FROM STUDENTS WHERE ID=?", id);
    }
}

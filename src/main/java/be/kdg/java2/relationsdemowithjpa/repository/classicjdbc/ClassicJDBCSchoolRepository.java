package be.kdg.java2.relationsdemowithjpa.repository.classicjdbc;

import be.kdg.java2.relationsdemowithjpa.domain.School;
import be.kdg.java2.relationsdemowithjpa.repository.SchoolRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Profile("classic")
public class ClassicJDBCSchoolRepository implements SchoolRepository {
    @Override
    public List<School> findAll() {
        return null;
    }

    @Override
    public School findById(int id) {
        return null;
    }

    @Override
    public School createSchool(School school) {
        return null;
    }

    @Override
    public void updateSchool(School school) {

    }

    @Override
    public void deleteSchool(int id) {

    }
}

package be.kdg.java2.relationsdemowithjpa.repository.jpa;

import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.domain.School;
import be.kdg.java2.relationsdemowithjpa.repository.SchoolRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceUnit;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Profile("jpa")
public class JPASchoolRepository implements SchoolRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<School> findAll() {
        List<School> schools = em.createQuery("select school from School school", School.class)
                .getResultList();
        return schools;
    }

    @Override
    public School findById(int id) {
        School school = em.find(School.class, id);
        return school;
    }

    @Override
    @Transactional
    public School createSchool(School school) {
        em.persist(school);
        return school;
    }

    @Override
    @Transactional
    public void updateSchool(School school) {
        em.merge(school);
    }

    @Override
    @Transactional
    public void deleteSchool(int id) {
        em.remove(em.find(School.class, id));
    }
}

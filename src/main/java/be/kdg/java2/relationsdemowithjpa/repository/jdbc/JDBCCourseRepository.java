package be.kdg.java2.relationsdemowithjpa.repository.jdbc;


import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.repository.CourseRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("jdbc")
public class JDBCCourseRepository implements CourseRepository {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert inserter;

    public JDBCCourseRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.inserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("COURSES")
                .usingGeneratedKeyColumns("ID");
    }

    //Helper method: maps the columns of the DB to the attributes of the Student
    public static Course mapCourseRow(ResultSet rs, int rowid) throws SQLException {
        return new Course(rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getInt("ACADEMIC_YEAR"));
    }

    @Override
    public List<Course> findAll() {
        //should we load the students for each course???
        //--> we will have to load addresses and schools also!
        //--> let's not do that...
        //--> this means the getStudents of course cannot be trusted?
        return jdbcTemplate.query("SELECT * FROM COURSES", JDBCCourseRepository::mapCourseRow);
    }

    @Override
    public Course findById(int id) {
        //should we load the students for this course???
        //--> it will load adresses and schools also!
        return jdbcTemplate.queryForObject("SELECT * FROM COURSES WHERE ID = ?", JDBCCourseRepository::mapCourseRow, id);
    }

    @Override
    public Course createCourse(Course course) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", course.getName());
        parameters.put("ACADEMIC_YEAR", course.getAcademicYear());
        course.setId(inserter.executeAndReturnKey(parameters).intValue());
        return course;
    }

    @Override
    public void updateCourse(Course course) {
        jdbcTemplate.update("UPDATE COURSES SET NAME=?, ACADEMIC_YEAR=? WHERE ID=?",
                course.getName(), course.getAcademicYear(), course.getId());
    }

    @Override
    @Transactional
    public void deleteCourse(int id) {
        //should we delete all the students of that course? No!
        //we should however delete the records from the STUDENTS_COURSES table!
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE COURSE_ID=?", id);
        jdbcTemplate.update("DELETE FROM COURSES WHERE ID=?", id);
    }
}

package be.kdg.java2.relationsdemowithjpa.service;

import be.kdg.java2.relationsdemowithjpa.domain.School;
import be.kdg.java2.relationsdemowithjpa.repository.SchoolRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("!springdata")
public class SchoolServiceImpl implements SchoolService{
    private SchoolRepository schoolRepository;

    public SchoolServiceImpl(SchoolRepository schoolRepository) {
        this.schoolRepository = schoolRepository;
    }

    @Override
    public List<School> getSchools() {
        return schoolRepository.findAll();
    }

    @Override
    public School getSchool(int schoolId) {
        return schoolRepository.findById(schoolId);
    }

    @Override
    public void deleteSchool(int schoolid) {
        schoolRepository.deleteSchool(schoolid);
    }

    @Override
    public School addSchool(School school) {
        return schoolRepository.createSchool(school);
    }
}

package be.kdg.java2.relationsdemowithjpa.service;

import be.kdg.java2.relationsdemowithjpa.domain.Student;

import java.util.List;

public interface StudentService {
    void setSchoolOfStudent(int studentId, int schoolId);
    Student addStudent(Student student);
    void changeStudent(Student student);
    void deleteStudent(int id);
    List<Student> getStudents();
    Student getStudent(int id);
    List<Student> getStudentsForCourse(int courseId);

    List<Student> getStudentsForSchool(int schoolid);

    void addCourseToStudent(int studentId, int courseId);
}

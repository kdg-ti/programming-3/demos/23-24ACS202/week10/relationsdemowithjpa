package be.kdg.java2.relationsdemowithjpa.service.springdata;

import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.repository.CourseRepository;
import be.kdg.java2.relationsdemowithjpa.repository.StudentRepository;
import be.kdg.java2.relationsdemowithjpa.repository.springdata.SpringDataCourseRepository;
import be.kdg.java2.relationsdemowithjpa.repository.springdata.SpringDataStudentRepository;
import be.kdg.java2.relationsdemowithjpa.service.CourseService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Profile("springdata")
public class SpringDataCourseService implements CourseService {
    private SpringDataCourseRepository courseRepository;
    private SpringDataStudentRepository studentRepository;

    public SpringDataCourseService(SpringDataCourseRepository courseRepository, SpringDataStudentRepository studentRepository) {
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    @Transactional
    public void deleteCourse(int id) {
        //student is in charge, so deleting the course will not delete the jointabel records
        //so we first have to remove the course from each student that follows it
        //then we can delete the course...
        Course course = courseRepository.findById(id).get();
        course.getStudents().forEach(student->{
            student.getCourses().remove(course);
            studentRepository.save(student);
        });
        courseRepository.deleteById(id);
    }

    @Override
    public List<Course> getCourses() {
        return courseRepository.findAll();
    }

    @Override
    public Course getCourse(int courseId) {
        return courseRepository.findById(courseId).get();
    }
}

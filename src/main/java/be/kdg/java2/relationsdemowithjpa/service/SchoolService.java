package be.kdg.java2.relationsdemowithjpa.service;

import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.domain.School;

import java.util.List;

public interface SchoolService {
    List<School> getSchools();
    School getSchool(int schoolId);

    void deleteSchool(int schoolid);

    School addSchool(School school);
}

package be.kdg.java2.relationsdemowithjpa.service.springdata;

import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.domain.School;
import be.kdg.java2.relationsdemowithjpa.domain.Student;
import be.kdg.java2.relationsdemowithjpa.repository.CourseRepository;
import be.kdg.java2.relationsdemowithjpa.repository.SchoolRepository;
import be.kdg.java2.relationsdemowithjpa.repository.StudentRepository;
import be.kdg.java2.relationsdemowithjpa.repository.springdata.SpringDataCourseRepository;
import be.kdg.java2.relationsdemowithjpa.repository.springdata.SpringDataSchoolRepository;
import be.kdg.java2.relationsdemowithjpa.repository.springdata.SpringDataStudentRepository;
import be.kdg.java2.relationsdemowithjpa.service.StudentService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Profile("springdata")
public class SpringDataStudentService implements StudentService {
    private SpringDataStudentRepository studentRepository;
    private SpringDataSchoolRepository schoolRepository;
    private SpringDataCourseRepository courseRepository;

    public SpringDataStudentService(SpringDataStudentRepository studentRepository, SpringDataSchoolRepository schoolRepository, SpringDataCourseRepository courseRepository) {
        this.studentRepository = studentRepository;
        this.schoolRepository = schoolRepository;
        this.courseRepository = courseRepository;
    }

    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    public void changeStudent(Student student){
        studentRepository.save(student);
    }

    @Override
    public void deleteStudent(int id) {
        studentRepository.deleteById(id);
    }

    @Override
    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student getStudent(int id) {
        return studentRepository.findById(id).get();
    }

    @Override
    public List<Student> getStudentsForCourse(int courseId) {
        return studentRepository.findByCourse(courseId);
    }

    @Override
    public List<Student> getStudentsForSchool(int schoolid) {
        return studentRepository.findBySchool(schoolid);
    }

    @Override
    @Transactional
    public void addCourseToStudent(int studentId, int courseId) {
        Student student = studentRepository.findById(studentId).get();
        Course course = courseRepository.findById(courseId).get();
        student.addCourse(course);
        studentRepository.save(student);
    }

    @Override
    @Transactional
    public void setSchoolOfStudent(int studentId, int schoolId) {
        Student student = studentRepository.findById(studentId).get();
        if (student!=null) {
            School school = schoolRepository.findById(schoolId).get();
            if (school!=null) {
                student.setSchool(school);
                studentRepository.save(student);
            }
        }
    }
}

package be.kdg.java2.relationsdemowithjpa.service.springdata;

import be.kdg.java2.relationsdemowithjpa.domain.School;
import be.kdg.java2.relationsdemowithjpa.repository.SchoolRepository;
import be.kdg.java2.relationsdemowithjpa.repository.springdata.SpringDataSchoolRepository;
import be.kdg.java2.relationsdemowithjpa.service.SchoolService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("springdata")
public class SpringDataSchoolService implements SchoolService {
    private SpringDataSchoolRepository schoolRepository;

    public SpringDataSchoolService(SpringDataSchoolRepository schoolRepository) {
        this.schoolRepository = schoolRepository;
    }

    @Override
    public List<School> getSchools() {
        return schoolRepository.findAll();
    }

    @Override
    public School getSchool(int schoolId) {
        return schoolRepository.findById(schoolId).get();
    }

    @Override
    public void deleteSchool(int schoolid) {
        schoolRepository.deleteById(schoolid);
    }

    @Override
    public School addSchool(School school) {
        return schoolRepository.save(school);
    }
}
